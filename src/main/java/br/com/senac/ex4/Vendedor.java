
package br.com.senac.ex4;

public class Vendedor {
    
    private int codigo;
    private String Nome;

    public Vendedor(int codigo, String Nome) {
        this.codigo = codigo;
        this.Nome = Nome;
    }

    public Vendedor() {
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }
    
}
