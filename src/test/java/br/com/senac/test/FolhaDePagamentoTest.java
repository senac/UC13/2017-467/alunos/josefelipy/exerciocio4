package br.com.senac.test;


import br.com.senac.ex4.FolhaDePagamento;
import br.com.senac.ex4.Produto;
import br.com.senac.ex4.Venda;
import br.com.senac.ex4.Vendedor;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;


public class FolhaDePagamentoTest {
    
    public FolhaDePagamentoTest() {
    }
    
    
    @Test
    public void deveCalcularZeroParaVendedorSemVendas(){
        Vendedor vendedor = new Vendedor(1, "Daniel");
        Venda venda = new Venda(vendedor);
        Vendedor vendedor2 = new Vendedor(2, "Jose");
        FolhaDePagamento folhaDePagamento = new FolhaDePagamento();
        List<Venda> listaVendas = new ArrayList<>();
        folhaDePagamento.calcularComissao(listaVendas);
        double comissao = folhaDePagamento.getComissaoVendedor(vendedor2);
        
        assertEquals(0, comissao , 0.001);
        
        
    }
    @Test
    public void deveCalcularComissaoParaJose(){
        Vendedor vendedor = new Vendedor(2, "José");
        
        Produto produto = new Produto(1, "Sabão");
    
        Venda venda1 = new Venda(vendedor);
        venda1.adicionarItem(produto, 1, 10);
        
        Venda venda2 = new Venda(vendedor);
        venda2.adicionarItem(produto, 1, 10);
        
        List<Venda> listaVendas = new ArrayList<>();
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaDePagamento folhaDePagamento = new FolhaDePagamento();
        folhaDePagamento.calcularComissao(listaVendas);
        double comissao = folhaDePagamento.getComissaoVendedor(vendedor);
        
        assertEquals(1, comissao, 0.01);
        
        
        
           
    }
    
    @Test
    public void deveCalcularComissaoParaJoseEFelipy(){
        Vendedor jose = new Vendedor(2, "José");
        Vendedor felipy = new Vendedor(3, "Felipy");
        
        Produto produto = new Produto(1, "Sabão");
    
        Venda venda1 = new Venda(jose);
        venda1.adicionarItem(produto, 1, 10);
        
        Venda venda2 = new Venda(felipy);
        venda2.adicionarItem(produto, 1, 10);
        
        List<Venda> listaVendas = new ArrayList<>();
        listaVendas.add(venda1);
        listaVendas.add(venda2);
        
        FolhaDePagamento folhaDePagamento = new FolhaDePagamento();
        folhaDePagamento.calcularComissao(listaVendas);
        double comissaoJose = folhaDePagamento.getComissaoVendedor(jose);
        double comissaoFelipy = folhaDePagamento.getComissaoVendedor(felipy);
        assertEquals(1 , comissaoJose + comissaoFelipy, 0.01);
        
        
    }
    @Test
    public void deveCalcularComissaoTotal(){
        Vendedor daniel = new Vendedor(1, "Daniel");
        Vendedor jose = new Vendedor(2, "José");
        Vendedor felipy = new Vendedor(3, "Felipy");
        
        Produto produto = new Produto(1, "Sabão");
        
        Venda venda1 = new Venda(daniel);
        venda1.adicionarItem(produto, 1, 10);
        
        Venda venda2 = new Venda(jose);
        venda2.adicionarItem(produto, 3, 10);
        
        Venda venda3 = new Venda(felipy);
        venda3.adicionarItem(produto, 2, 10);
        
         List<Venda> listaVendas = new ArrayList<>();
         listaVendas.add(venda1);
         listaVendas.add(venda2);
         listaVendas.add(venda3);
        
        FolhaDePagamento folhaDePagamento = new FolhaDePagamento();
        folhaDePagamento.calcularComissao(listaVendas);
        double comissaoDaniel = folhaDePagamento.getComissaoVendedor(daniel);
        double comissaoJose = folhaDePagamento.getComissaoVendedor(jose);
        double comissaoFelipy = folhaDePagamento.getComissaoVendedor(felipy);
        
        assertEquals(3 ,comissaoDaniel + comissaoFelipy + comissaoJose, 0.01);
        
    }
    
}
